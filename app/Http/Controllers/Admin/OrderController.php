<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Order;
class OrderController extends Controller
{
public function index()  
{
    return view('admin/order/index')->withOrder(Order::all());
}
}
