<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommonUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('common_user', function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string('wx_id'); 
            $table->string('mobile', 13);
            $table->string('sex');
            $table->string('address');
            $table->integer('post_id');
            $table->integer('top_level');
            $table->integer('second_level');
            $table->integer('third_level');
            $table->integer('total_integral');
            $table->timestamps();          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
