<!DOCTYPE html>  
<html lang="en">  
<head>  
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>微商进销存</title>

    <link href="//cdn.bootcss.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
    <script src="//cdn.bootcss.com/jquery/1.11.1/jquery.min.js"></script>
    <script src="//cdn.bootcss.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="container">
        <div id="content">
            <ul>
                @foreach ($order as $orders)
                <li style="margin: 50px 0;">
                    <div class="title">
                        <a href="{{ url('article/'.$orders->id) }}">
                            <h4>{{ $orders->user_id }}</h4>
                        </a>
                    </div>
                    <div class="body">
                        <p>{{ $orders->price }}</p>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
</body>
</html>
